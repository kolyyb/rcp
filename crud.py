import sqlite3

conn = sqlite3.connect('sample.db', check_same_thread=False)
cur = conn.cursor()


# On récupère tous les organismes certificateurs
def get_all_certificateurs():
     cur.execute(""" SELECT libelle_certificateur FROM certificateur;""")
     return cur.fetchall()


# On recupére tous les types de diplômes
def get_all_diplomes():
     cur.execute(""" SELECT libelle_diplome FROM diplome;""")
     return cur.fetchall()