from typing import List

from starlette import schemas

from Models.Models import Certificateur, Diplome
from crud import get_all_certificateurs, get_all_diplomes
from fastapi import FastAPI

description = """
API qui interroge un bdd des Niveaux de prise en charge 
financieres des formations par certification et par CPNE. 🚀


On peut : 
* Récupérer **tous les certificateurs**
* Récupérer **tous les diplômes**
* Récupérer **tous le**s codes Identifiant Des Conventions CollectivesIDCC des entreprises (IDCC)
* Récupérer **toutes Les commissions paritaire nationale emploi et 
formation professionnelle (CPNEFP ou CPNEF ou CPNE)**
* Récupérer **tous les montants NPEC**
"""
tags_metadata = [
    {
        "name": "certificateur",
        "description": "Récupère une liste de tous les certificateurs"
    },
    {
        "name": "diplome",
        "description": "Réucupere une liste de tous les noms de diplomes.",
        }
]

app = FastAPI(
    title="Base de données des niveaux de prise en charge financières des formations",
    description=description,
    version="1.0.0",
    openapi_tags=tags_metadata
)


@app.get("/certificateur", tags=["certificateur"], response_model=List[Certificateur], summary="recupere la liste des certificats")
def get_all_certificateur() -> List[Certificateur]:
    """"""
    response = get_all_certificateurs()

    return [Certificateur(libelle_certificateur=result[0]) for result in response]


@app.get("/diplome", tags=["diplome"], response_model=List[Diplome])
def get_all_diplome() -> List[Diplome]:
    response = get_all_diplomes()
    return [Diplome(libelle_diplome=result[0]) for result in response]







if __name__ == "__main__":
    pass
