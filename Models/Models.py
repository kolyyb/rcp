from pydantic import BaseModel


class Certificateur(BaseModel):
    libelle_certificateur:str


class Diplome(BaseModel):
    libelle_diplome:str

