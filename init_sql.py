import sqlite3
from xmlrpc.client import DateTime

conn = sqlite3.connect('sample.db', check_same_thread=False)
cur = conn.cursor(dict)


def sql_structure_certificateur():
    cur.execute("""CREATE TABLE IF NOT EXISTS 'certificateur' (
    'id'	INTEGER,
    'libelle_certificateur'	TEXT NOT NULL UNIQUE,
    PRIMARY KEY (id AUTOINCREMENT)
);""")
    conn.commit()


def sql_structure_statut():
    cur.execute("""CREATE TABLE IF NOT EXISTS 'statut'(
    'id' INTEGER,
    'libelle_statut' TEXT NOT NULL UNIQUE,
    PRIMARY KEY (id AUTOINCREMENT)
 );""")
    conn.commit()



def sql_structure_diplome():
    cur.execute("""CREATE TABLE IF NOT EXISTS 'diplome'(
    'id' INTEGER,
    'libelle_diplome' TEXT NOT NULL UNIQUE,
    PRIMARY KEY (id AUTOINCREMENT)
 );""")
    conn.commit()



def sql_structure_idcc():
    cur.execute("""CREATE TABLE IF NOT EXISTS 'idcc'(
    'code_idcc' INTEGER,
    'id_cpne' INTEGER,
    FOREIGN KEY (id_cpne) REFERENCES cpne(id),
    PRIMARY KEY (code_idcc)
 );""")
    conn.commit()



def sql_structure_cpne():
    cur.execute("""CREATE TABLE IF NOT EXISTS 'cpne'(
    'id' INTEGER,
    'libelle_cpne' TEXT NOT NULL UNIQUE,
    PRIMARY KEY (id AUTOINCREMENT)
 );""")
    conn.commit()



def sql_structure_npec():
    cur.execute("""CREATE TABLE IF NOT EXISTS 'npec'(
    'id' INTEGER,
    'npec_montant' INTEGER,
    'npec_date' DATE,
    PRIMARY KEY (id AUTOINCREMENT)
 );""")
    conn.commit()


def sql_structure_rncp():
    cur.execute("""CREATE TABLE IF NOT EXISTS 'rncp'(
    'id' INTEGER,
    'code_rncp' TEXT NOT NULL,
    'libelle_formation' TEXT,
    'id_diplome' INTEGER,
    'id_certificateur' INTEGER,
    FOREIGN KEY (id_diplome) REFERENCES diplome(id),
    FOREIGN KEY (id_certificateur) REFERENCES certificateur(id),
    PRIMARY KEY (id AUTOINCREMENT)
    );""")
    conn.commit()



def sql_structure_relations():
    cur.execute("""CREATE TABLE IF NOT EXISTS 'relations'(
    'id_rncp' INTEGER,
    'id_cpne' INTEGER,
    'id_npec' INTEGER,
    'id_status' INTEGER,
    FOREIGN KEY (id_rncp) REFERENCES rncp(id),
    FOREIGN KEY (id_cpne) REFERENCES cpne(id),
    FOREIGN KEY (id_npec) REFERENCES npec(id),
    FOREIGN KEY (id_status) REFERENCES statut(id)
 );""")
    conn.commit()



def insert_statut(statut: str):
    cur.execute("""SELECT id FROM statut WHERE libelle_statut = (?)""", (statut,))
    founded_value = cur.fetchone()
    id_statut = founded_value[0] if founded_value else 0
    if founded_value is None:
        cur.execute("""INSERT INTO statut(libelle_statut) VALUES (?)""", (statut,))
        conn.commit()
        id_statut = cur.lastrowid
    return id_statut


def insert_diplome(diplome: str):
    cur.execute("""SELECT id FROM diplome WHERE libelle_diplome = (?)""", (diplome,))
    founded_value = cur.fetchone()
    id_diplome = founded_value[0] if founded_value else 0
    if founded_value is None:
        cur.execute("""INSERT INTO diplome(libelle_diplome) VALUES (?)""", (diplome,))
        conn.commit()
        id_diplome = cur.lastrowid
    return id_diplome


def insert_npec(npec_montant: int, npec_date: str):
    cur.execute("""SELECT id FROM npec WHERE npec_montant = (?)""", (npec_montant,))
    founded_value = cur.fetchone()
    id_npec = founded_value[0] if founded_value else 0
    if founded_value is None:
        cur.execute("""INSERT INTO npec(npec_montant, npec_date) VALUES (?, ?)""", [npec_montant, DateTime(npec_date).value])
        conn.commit()
        id_npec = cur.lastrowid
    return id_npec


def insert_certificateur(certificateur: str):
    cur.execute("""SELECT id FROM certificateur WHERE libelle_certificateur = (?)""", (certificateur,))
    founded_value = cur.fetchone()
    id_certificateur = founded_value[0] if founded_value else 0
    if founded_value is None:
        cur.execute("""INSERT INTO certificateur(libelle_certificateur) VALUES (?)""", (certificateur,))
        conn.commit()
        id_certificateur = cur.lastrowid
    return id_certificateur


def insert_cpne(libelle_cpne: str, id: int):
    cur.execute("""SELECT id FROM cpne WHERE libelle_cpne = (?) AND id = (?)""", (libelle_cpne, id))
    founded_value = cur.fetchone()
    id_cpne = founded_value[0] if founded_value else 0
    if founded_value is None:
        cur.execute("""INSERT INTO cpne(id, libelle_cpne) VALUES (?, ?)""", (id, libelle_cpne))
        conn.commit()
        id_cpne = cur.lastrowid
    return id_cpne


def insert_idcc(code_idcc: int, id_cpne: int):
    cur.execute("""SELECT code_idcc FROM idcc WHERE code_idcc = (?) AND id_cpne = (?)""", (code_idcc, id_cpne,))
    founded_value = cur.fetchone()
    id_idcc = founded_value[0] if founded_value else 0
    if founded_value is None:
        cur.execute("""INSERT INTO idcc(code_idcc, id_cpne) VALUES (?, ?)""", (code_idcc, id_cpne))
        conn.commit()
        id_idcc = cur.lastrowid
    return id_idcc


def insert_rncp(code_rncp:str, libelle_formation:str, id_diplome:int, id_certificateur:int):
    cur.execute("""SELECT id FROM rncp WHERE code_rncp = (?) AND libelle_formation = (?) AND id_diplome = (?) AND id_certificateur = (?)""",
                          ( code_rncp, libelle_formation, id_diplome, id_certificateur))
    founded_value = cur.fetchone()
    id_rncp = founded_value[0] if founded_value else 0
    if founded_value is None:
        cur.execute("""INSERT INTO rncp(code_rncp, libelle_formation, id_diplome, id_certificateur) VALUES (?, ?, ?, ?)""",
                    (code_rncp, libelle_formation, id_diplome, id_certificateur))
        conn.commit()
        id_rncp = cur.lastrowid
    return id_rncp


def insert_relations(id_rncp:int, id_cpne:int, id_npec:int, id_status:int):
    cur.execute("""SELECT * FROM relations WHERE id_rncp = (?) AND id_cpne = (?) AND id_npec = (?) AND id_status = (?)""",
                ( id_rncp, id_cpne, id_npec, id_status))
    founded_value = cur.fetchone()
    if founded_value is None:
        cur.execute("""INSERT INTO relations(id_rncp, id_cpne, id_npec, id_status) VALUES(
        ?, ?, ?, ?)""", (id_rncp, id_cpne, id_npec, id_status))
        conn.commit()

