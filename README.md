# Référentiel unique de prise en charge des contrats d'apprentissage

## Mise à disposition d'un fichier très volumineux d'un référentiels au format *.xlsx

### La demande : 

- [x] lire toutes les données de ce gros fichier
- [x] extraire les données de ce fichier
- [x] normaliser les données
- [x] réinjecter ces données dans une base de donnes sqlite
- [ ] développer une API pour requêter cette nouvelle base de données
- [ ] développer une IHM pour consulter les résultats de ces requêtes

### Stack technique :
* pandas
* python
* sqlite3
### MCD
![Modele Conceptuel de Données](./mcd_cnep.png "Modele conceptuel de données").

### MPD
![Modele physique de Données](./mpd_cnep.png "Modele physique de données").

### TODO :
* optimiser le temps d'insertion des données dans la bdd (_1h40 en l'etat_)
 * **Pistes :**
   * utiliser l'asynchrone ?