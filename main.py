import pandas as pd
import sqlite3
import cProfile



from init_sql import (sql_structure_certificateur, sql_structure_statut, sql_structure_relations,
                      sql_structure_cpne, sql_structure_diplome, sql_structure_npec, insert_statut,
                      insert_certificateur, sql_structure_rncp, sql_structure_idcc, insert_cpne,
                      insert_idcc, insert_npec, insert_diplome, insert_rncp, insert_relations)

filename = "./Referentiel-des-NPEC.2024.xlsx"


sql_structure_certificateur()
sql_structure_statut()
sql_structure_cpne()
sql_structure_idcc()
sql_structure_diplome()
sql_structure_npec()
sql_structure_rncp()
sql_structure_relations()

df = pd.read_excel(filename, engine='openpyxl', sheet_name='Onglet 3 - référentiel NPEC', header=3)
df2 = pd.read_excel(filename, engine='openpyxl', sheet_name='Onglet 4 - CPNE-IDCC', header=2)

conn = sqlite3.connect('sample.db', check_same_thread=False)
cur = conn.cursor()


def insert_data():
    row = df2.to_dict('records')
    for value in row:
        id_cpne = insert_cpne(id=value['Code CPNE'], libelle_cpne=value['CPNE'])
        id_idcc = insert_idcc(code_idcc=value['IDCC'], id_cpne=id_cpne)

    table = df.to_dict('records')
    for value in table:
        id_statut = insert_statut(statut=value['Statut'])
        id_diplome = insert_diplome(diplome=value['Libellé du Diplôme'].lower())

        id_npec = insert_npec(npec_montant=value['NPEC final'], npec_date=value['Date d\'applicabilité des NPEC** \n(**hors contrats couverts par la valeur d\'amorçage + voir onglet "Me lire")'])

        id_cpne = insert_cpne(id=value['Code CPNE'], libelle_cpne=value['CPNE'])
        id_certificateur = insert_certificateur(certificateur=
                                                value[
                                                    "Certificateur* \n(*liste non exhaustive - premier dans la fiche RNCP)"].lower())

        id_rncp = insert_rncp(code_rncp=value['Code RNCP'],libelle_formation=value['Libellé de la formation'],
                              id_diplome=id_diplome, id_certificateur=id_certificateur)

        insert_relations(id_rncp, id_cpne, id_npec, id_statut)




if __name__ == '__main__':
    with cProfile.Profile() as profile:
        profile.enable()
        insert_data()
        profile.disable()
        profile.print_stats()



